jQuery(document).ready(function($){

/*-------- Input Animations ----------*/

	$('.input__field').focusout(function(){ 
		if($.trim($(this).val()) == 0) {
			$(this).parent().removeClass('input--filled');
		} 
	});

	$('.input__field').focusin(function(){
		$(this).parent().addClass('input--filled');
	});
  
/*------------------------------------ */

/*-------- Burger Animations ----------*/

	$('#burger').click(function() {
		$('body').toggleClass('open-menu');
	});

/*------------------------------------ */
/*------------Slick slider------------ */

	$('#gift-slider').slick({
		arrows: true,
		slidesToShow: 4,
		slidesToScroll: 1,
		responsive: [
		 {
	      breakpoint: 960,
	      settings: {
	        slidesToShow: 3,
	        arrows: false,
	        slidesToScroll: 1  
	      }
	    },
	    {
	      breakpoint: 640,
	      settings: {
	        arrows: false,
	        slidesToShow: 2,
	        slidesToScroll: 1  
	      }
	    },
	    {
	      breakpoint: 420,
	      settings: {
	        slidesToShow: 1,
	        slidesToScroll: 1,
	        arrows: false,
            centerMode: true,
            centerPadding: '20px',
	      }
	    }],
	}); 
/*------------------------------------ */  
});

